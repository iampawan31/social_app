@extends('layouts.master')
@section('pagename')
    accounts
@endsection
@section('title')
    Account | Social App
@endsection
@section('content')
    <section class="row account">
        <div class="col-md-6">
            <img src="{{url('media/large/'. $user->profile_image)}}" alt="" class="img-responsive">
        </div>
        <div class="col-md-6">
            <header>
                <h3>Your Account</h3>
            </header>
            <form action="{{route('account.update')}}" method="post" enctype="multipart/form-data">
                <input type="hidden" value="{{Session::token()}}" name="_token">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" value="{{$user->name}}" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="image">Profile Image (.jpg only)</label>
                    <input type="file" name="profile_image" id="image">
                </div>
                <button type="submit" class="btn btn-primary">Update Account</button>
            </form>
        </div>
    </section>
@endsection
@section('optionaljs')
    {{--<script src="{{asset('src/js/dashboard.js')}}"></script>--}}
@endsection