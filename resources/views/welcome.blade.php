@extends('layouts.startpagemaster')
@section('pagename')
    homepage
@endsection
@section('title')
    Home | Social App
@endsection
@section('content')
    <div class="row homepage-main-block">
        <div class="col-md-6">
            <h1>Social Network Built for Developers</h1>
        </div>
        <div class="col-md-4" id="signup-login-block">
            <div class="signup-block" v-show="signupShow">
                <h1>Sign Up</h1>

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form @submit.prevent="submitSignUpForm" action="{{route('signup')}}" method="post" id="signupform">
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control" name="email" id="email" required
                               value="{{Request::old('email')}}" v-model="signUpEmail">
                        <span v-if="formSignUpErrors['email']" class="error">@{{ formSignUpErrors['email'] }}</span>
                    </div>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" id="name" required
                               value="{{Request::old('name')}}" v-model="signUpName">
                        <span v-if="formSignUpErrors['name']" class="error">@{{ formSignUpErrors['name'] }}</span>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" name="password" id="password" required
                               v-model="signUpPassword">
                        <span v-if="formSignUpErrors['password']"
                              class="error">@{{ formSignUpErrors['password'] }}</span>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Sign Up</button>
                        <a href="#" id="signup" v-on:click="toggleLogin($event)">Already have an Account?</a>
                    </div>
                </form>
            </div>
            <div class="login-block" v-show="loginShow">
                <h1>Login</h1>
                @if(Session::has('loginmessage'))
                    <span class="alert">{{ Session::get('loginmessage') }}</span>
                @endif
                <span v-if="ajaxSignInCheckError" class="error">Username/Password is Incorrect. Please try again.</span>
                <form @submit.prevent="submitLoginForm" action="{{route('signin')}}" method="post">
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control" name="email" id="email"
                               value="{{Request::old('email')}}" v-model="signInEmail">
                        <span v-if="formSignInErrors['email']" class="error">@{{ formSignInErrors['email'] }}</span>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" name="password" id="password"
                               v-model="signInPassword">
                        <span v-if="formSignInErrors['password']"
                              class="error">@{{ formSignInErrors['password'] }}</span>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Login</button>
                        <a href="#" id="login" v-on:click="toggleSignup($event)"><span
                                    class="glyphicon glyphicon-menu-left">Back</span></a>
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection
@section('optionaljs')
    <script src="{{asset('src/js/app.js')}}"></script>
@endsection