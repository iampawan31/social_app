<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>@yield('title')</title>
    <link href="{{asset('src/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('src/css/custom.css')}}" rel="stylesheet">
</head>
<body class="@yield('pagename')">
<div class="container ">
    @yield('content')
</div>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{asset('src/js/jquery.min.js')}}"></script>
<script src="{{asset('src/js/bootstrap.min.js')}}"></script>
<script src="{{asset('src/js/vue.js')}}"></script>
<script src="{{asset('src/js/vue-resource.min.js')}}"></script>
@yield('optionaljs')
</body>
</html>
