@extends('layouts.master')
@section('pagename')
    dashboard
@endsection
@section('title')
    Dashboard | Social App
@endsection
@section('content')
    <section class="row new-post">
        <div class="col-md-12">
            <header>
                <h3>What's on your mind?</h3>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if(session('poststatus'))
                    <span class="label-success">{{session('poststatus')}}</span>
                @endif
            </header>
            <form action="{{route('post.create')}}" method="post">
                <div class="formgroup">
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                    <textarea name="newpost" id="new-post-content" cols="30" rows="10" class="form-control"></textarea>
                </div>
                <button class="btn btn-primary">Post</button>
            </form>
        </div>
    </section>
    <section class="row posts" id="posts-block">
        @foreach($posts as $post)
            <div class="col-md-1">
                <img src="{{url('media/small/'. $post->user->profile_image)}}" alt="" class="img-responsive">
            </div>
            <div class="col-md-11">
                <article class="post">
                    <p>{{$post->post_content}}</p>
                    <div class="info">
                        -{{$post->user->name}} on {{date('D, d M Y, h:ia', strtotime($post->created_at))}}
                    </div>
                    <div class="post-actions">
                        @if(Auth::user() != $post->user)

                            <a href="{{route('like')}}" v-on:click="likepost" data-token="{{Session::token()}}"
                               data-postid="{{$post->id}}">Like</a> |
                            <a href="{{route('like')}}" v-on:click="commentpost" data-token="{{Session::token()}}"
                               data-postid="{{$post->id}}">Dislike</a>
                        @endif
                        @if(Auth::user() == $post->user)
                            <a href="{{route('post.edit')}}" v-on:click="editpost" data-token="{{Session::token()}}"
                               data-postid="{{$post->id}}">Edit</a>
                            |
                            <a href="#">Delete</a>
                        @endif
                    </div>
                </article>
            </div>
        @endforeach
        <div class="modal fade" tabindex="-1" role="dialog" id="editpost">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Post</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                        <div class="form-group">
                            <textarea type="textarea" cols="30" rows="10" class="form-control" v-model="editPost"
                                      name="postcontent" required></textarea>
                            <span v-if="formEditErrors['postcontent']"
                                  class="error">@{{ formEditErrors['postcontent'] }}</span>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" v-on:click="updatepost">Save changes</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </section>

@endsection
@section('optionaljs')
    <script src="{{asset('src/js/dashboard.js')}}"></script>
@endsection