<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use GrahamCampbell\Flysystem\Facades\Flysystem;
use Intervention\Image\Facades\Image;
use League\Glide;

class UserController extends Controller
{
    public function postSignUp(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users',
            'name' => 'required|max:120',
            'password' => 'required|min:6'
        ]);

        if ($request->ajax()) return;

        $email = $request['email'];
        $name = $request['name'];
        $password = bcrypt($request['password']);

        $user = new User();
        $user->email = $email;
        $user->name = $name;
        $user->password = $password;
        $user->save();

        Auth::login($user);

        return redirect()->route('dashboard');

    }

    public function postSignIn(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);

        if ($request->ajax()) {
            if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {
                $data = Array('ajaxloginstatus' => true);
                return $data;
            } else {
                $data = Array('ajaxloginstatus' => false);
                return $data;
            }
        } else {
            if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {
                return redirect()->route('dashboard');
            } else {
                $request->session()->flash('loginmessage', "Login Failed. Username Or Password is incorrect");
                return redirect()->back();
            }
        }

        return redirect()->back();
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect()->route('home');
    }

    public function getAccount()
    {
        $user = Auth::user();

//        dd($image = Image::make(storage_path(). '/' .$user->profile_image));

        return view('account', ['user' => Auth::user()]);
    }

    public function postUpdateAccount(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:120'
        ]);

        $user = Auth::user();
        $file = $request->file('profile_image');
        $file_extension = $file->getClientOriginalExtension();
        $file = Image::make($file);
        $filename = $request['name'] . '-' . $user->id . '.' . $file_extension;
        $profile_image = Flysystem::put('files/' . $filename, $file->stream());
        if ($profile_image) {
            $user->profile_image = $filename;
        }
        $user->name = $request['name'];
        $user->update();

        return redirect()->route('account');

    }


}
