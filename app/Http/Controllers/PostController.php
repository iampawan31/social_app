<?php
/**
 * Created by PhpStorm.
 * User: Warlock
 * Date: 7/23/2016
 * Time: 3:18 AM
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Redis;
use App\Post;
use App\Like;
use App\User;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function getDashboard()
    {
        $posts = Post::orderBy('created_at', 'desc')->get();
        return view('dashboard', ['posts' => $posts]);
    }

    public function postCreatePost(Request $request)
    {
        $this->validate($request, [
            'newpost' => 'required|max:1000'
        ]);

        $newPost = $request['newpost'];

        $post = new Post();
        $post->post_content = $newPost;

        if ($request->user()->posts()->save($post)) {
            $request->session()->flash('poststatus', 'Post Creation Success');
        } else {
            $request->session()->flash('poststatus', 'Post Creation Failed');
        }


        return redirect()->route('dashboard');
    }

    public function getDeletePost($post_id)
    {
        $post = Post::where('id', $post_id)->first();
        if (Auth::user() != $post->user) {
            return redirect()->back();
        }
        $post->delete();
        return redirect()->route('dashboard')->with(['message' => 'Post Deleted']);
    }

    public function postEditPost(Request $request)
    {
        $this->validate($request, [
            'postcontent' => 'required'
        ]);

        $post = Post::find($request['postid']);
        if (Auth::user() != $post->user) {
            return redirect()->back();
        }
        $post->post_content = $request['postcontent'];
        $post->update();
        return response()->json(['message' => 'Post Updated', 'updatedcontent' => $request['postcontent']], 200);
    }

    public function postLikePost(Request $request)
    {
        $postId = $request['postId'];
        $isLike = $request['isLike'] === true ? true : false;
        $update = false;

        $post = Post::find($postId);
        if (!$post) {
            return null;
        }
        $user = Auth::user();
        $like = $user->likes()->where('post_id', $postId)->first();

        if ($like) {
            $alreadyLike = $like->like;
            $update = true;

            if ($alreadyLike == $isLike) {
                $like->delete();
                return null;
            }
        } else {

            $like = new Like();
        }
        $like->like = $isLike;
        $like->user_id = $user->id;
        $like->post_id = $post->id;
        if ($update) {
            $like->update();
        } else {
            $like->save();
        }

        return null;


    }


}