<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['middleware' => ['web']], function () {

    Route::get('/', function () {
        if (Auth::check()) {
            return redirect()->route('dashboard');
        }
        return view('welcome');
    })->name('home');

    Route::post('/signup', [
        'uses' => 'UserController@postSignUp',
        'as' => 'signup'
    ]);

    Route::post('/signin', [
        'uses' => 'UserController@postSignIn',
        'as' => 'signin'
    ]);

    Route::get('/logout', [
        'uses' => 'UserController@getLogout',
        'as' => 'logout'
    ]);

    Route::get('/account', [
        'uses' => 'UserController@getAccount',
        'as' => 'account',
        'middleware' => 'auth'
    ]);

    Route::post('/updateaccount', [
        'uses' => 'UserController@postUpdateAccount',
        'as' => 'account.update'
    ]);

    Route::get('/dashboard', [
        'uses' => 'PostController@getDashboard',
        'as' => 'dashboard',
        'middleware' => 'auth'
    ]);

    Route::post('/createpost', [
        'uses' => 'PostController@postCreatePost',
        'as' => 'post.create',
        'middleware' => 'auth'
    ]);

    Route::get('/delete-post/{post_id}', [
        'uses' => 'PostController@getDeletePost',
        'as' => 'post.delete',
        'middleware' => 'auth'
    ]);

    Route::post('/edit', [
        'uses' => 'PostController@postEditPost',
        'as' => 'post.edit',
        'middleware' => 'auth'
    ]);

    Route::post('/like', [
        'uses' => 'PostController@postLikePost',
        'as' => 'like',
    ]);


    Route::get('media/{template}/{filename}', function ($template, $filename) {

        switch ($template) {
            case 'small':
                $width = 200;
                break;

            case 'medium':
                $width = 400;
                break;
            case 'large':
                $width = 600;
                break;
        }
//        if(!File::exists( $image=storage_path("files/{$image}") )) abort(404);
//        if(!File::exists( $image=Flysystem::read("files/{$image}") )) abort(404);
        $filename = Flysystem::read("files/{$filename}");
        $returnImage = Image::make($filename)->resize($width, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        return $returnImage->response();
    });
});