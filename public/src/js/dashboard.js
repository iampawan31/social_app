var vm = new Vue({
    el: "#posts-block",
    data: {
        signInPassword: '',
        ajaxSignInCheckError: false,
        formEditInputs: {},
        formEditErrors: {},
        editPostModal: {},
        editPost: {},
        editPostToken: {},
        editToken: {},
        editRoute: {},
        editPostId: {},
        postContent: {},
        likecommentToken: {},
        likecommentPostId: {},
        likecommentRoute: {},
        isLike: {}
    },
    methods: {
        editpost: function (event) {
            event.preventDefault();

            this.postContent = event.target.parentNode.parentNode.childNodes[1];
            this.editRoute = event.target.href;
            this.editToken = event.target.getAttribute('data-token');
            this.editPostId = event.target.getAttribute('data-postid');
            this.editPost = this.postContent.textContent;
            // console.log();
            $('#editpost').modal();
        },
        updatepost: function (event) {
            event.preventDefault();
            this.formEditInputs = {
                postcontent: this.editPost,
                postid: this.editPostId,
                _token: this.editToken
            };

            // console.log(this.editRoute);

            this.$http.post(this.editRoute, this.formEditInputs, {
                    headers: {
                        'X-CSRF-TOKEN': this.editToken
                    }
                })
                .then(function (data) {
                    console.log(data.data.updatedcontent);
                    $(this.postContent).html(data.data.updatedcontent);
                    $('#editpost').modal('hide');

                    // console.log(data.body);
                })
                .catch(function (data, status, request) {
                    // console.log("going to catch" + data);
                    if (data > 0) {
                        var errors = data.data;
                    } else {
                        this.formEditErrors = "";
                    }
                });

        },
        likepost: function (event) {
            event.preventDefault();
            console.log("clicked");
            this.likecommentRoute = event.target.href;
            this.likecommentToken = event.target.getAttribute('data-token');
            this.likecommentPostId = event.target.getAttribute('data-postid');
            this.isLike = true;

            this.$http.post(this.likecommentRoute, {
                    postId: this.likecommentPostId,
                    isLike: this.isLike,
                    _token: this.likecommentToken
                }, {
                    headers: {
                        'X-CSRF-TOKEN': this.likecommentToken
                    }
                })
                .then(function (data) {
                    event.target.innerText = event.target.innerText === 'Like' ? 'You Like this post' : 'Like';
                })
                .catch(function (data, status, request) {

                });
        },
        commentpost: function (event) {
            event.preventDefault();
            console.log("Clicked");
            this.likecommentRoute = event.target.href;
            this.likecommentToken = event.target.getAttribute('data-token');
            this.likecommentPostId = event.target.getAttribute('data-postid');
            this.isLike = false;

            this.$http.post(this.likecommentRoute, {
                    postId: this.likecommentPostId,
                    isLike: this.isLike,
                    _token: this.likecommentToken
                }, {
                    headers: {
                        'X-CSRF-TOKEN': this.likecommentToken
                    }
                })
                .then(function (data) {
                    event.target.innerText = event.target.innerText === 'Dislike' ? 'You don\'t Like this post' : 'DisLike';
                })
                .catch(function (data, status, request) {

                });
        }
    }


});
